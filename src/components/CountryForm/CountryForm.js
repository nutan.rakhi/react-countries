import React, { useState } from "react";
import "./CountryForm.css";

//Dropdowns
import CountriesDropdown from "../Dropdowns/CountriesDropdown";
import StatesDropdown from "../Dropdowns/StatesDropdown";
import CitiesDropdown from "../Dropdowns/CitiesDropdown";

//Cards
import ModalDetails from "../Modals/ModalDetails";

//semantic
import { Header, Form, Segment } from "semantic-ui-react";

const CountryForm = () => {
  const [country, setCountry] = useState({ key: null });
  const [state, setState] = useState({ key: null });
  const [city, setCity] = useState({ key: null });
  const [pinCode, setPincode] = useState("");

  const onCountriesDropdownChange = (data) => {
    setCountry(data);
    setState({ key: null });
    setCity({ key: null });
  };

  const onStatesDropdownChange = (data) => {
    setState(data);
    setCity({ key: null });
  };

  const onCitiesDropdownChange = (data) => {
    setCity(data);
  };

  return (
    <div style={{ width: "90%", margin: "0 auto" }}>
      <Segment placeholder>
        <Header
          style={{
            marginTop: 0,
          }}
        >
          COUNTRIES FORM
        </Header>
        <Form>
          <Form.Group widths="equal">
            <CountriesDropdown onDropdownChange={onCountriesDropdownChange} />
            <StatesDropdown
              onDropdownChange={onStatesDropdownChange}
              country={country.key}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <CitiesDropdown
              onDropdownChange={onCitiesDropdownChange}
              state={state.key}
            />
            <Form.Input
              fluid
              placeholder="Pin Code"
              value={pinCode}
              onChange={(e) => setPincode(e.target.value)}
            />
          </Form.Group>
          <ModalDetails
            country={country.text}
            flag={country.flag}
            state={state.text}
            city={city.text}
            pinCode={pinCode}
          />
        </Form>
      </Segment>
    </div>
  );
};

export default CountryForm;
