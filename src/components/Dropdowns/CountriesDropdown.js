import React, { useEffect, useState } from "react";

import world from "../../api/world";

import { Dropdown } from "semantic-ui-react";

const CountriesDropdown = (props) => {
  const [countries, setCountries] = useState([]);

  const getCountry = (data) => {
    let country_id = data.value;
    return countries.find((country) => country.key === country_id);
  };

  useEffect(() => {
    const getCountries = async () => {
      const response = await world.get("/countries/read", {});

      setCountries(
        response.data.countries.map((country) => {
          let { iso2, name, id } = country;
          iso2 = iso2.toLowerCase();
          return {
            key: id,
            value: id,
            flag: iso2,
            text: name,
          };
        })
      );
    };
    getCountries();
  }, []);

  return (
    <Dropdown
      placeholder="Select Country"
      fluid
      search
      selection
      options={countries}
      onChange={(e, data) => props.onDropdownChange(getCountry(data))}
    />
  );
};

export default CountriesDropdown;
