import React, { useEffect } from "react";
import { Button, Header, Image, Modal } from "semantic-ui-react";

const ModalDetails = (props) => {
  const { country, state, city, pinCode, flag } = props;
  const [open, setOpen] = React.useState(false);
  const [disabled, setDisabled] = React.useState(true);

  useEffect(() => {
    if (country == null || state == null || city == null || pinCode === "") {
      setDisabled(true);
    } else setDisabled(false);
  }, [country, state, city, pinCode]);

  return (
    <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button disabled={disabled}>Submit</Button>}
    >
      <Modal.Header>Details</Modal.Header>
      <Modal.Content image>
        <Image
          size="medium"
          src={`https://countryflagsapi.com/png/${flag}`}
          wrapped
        />
        <Modal.Description>
          <Header>{country}</Header>
          <p>
            <b>State:</b> {state}
          </p>
          <p>
            <b>City:</b> {city}
          </p>
          <p>
            <b>Pincode:</b> {pinCode}
          </p>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button color="red" onClick={() => setOpen(false)}>
          Cancel
        </Button>
        <Button
          content="Yes, Submit"
          labelPosition="right"
          icon="checkmark"
          onClick={() => setOpen(false)}
          positive
        />
      </Modal.Actions>
    </Modal>
  );
};

export default ModalDetails;
